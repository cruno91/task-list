# Task List #

This is my first native iPhone app.  Be nice.

### About ###

* Add tasks to a list, remove tasks from a list.
* Version: 0.1a

### Set up ###

* Clone git repository
* Open project in XCode

### TODO ###

* Write tests
* Add dates
* Add time-based reminders
* Add location-based reminders
* Add cloud sync capabilities

### Contact ###

* chris.runo@gmail.com