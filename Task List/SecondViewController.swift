//
//  SecondViewController.swift
//  Task List
//
//  Created by Chris Runo on 1/12/15.
//  Copyright (c) 2015 cron. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtTask: UITextField!
    @IBOutlet var txtDesc: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Events
    @IBAction func btnAddTask_Click(sender: UIButton) {
        // Add task to array from first view controller
        taskMgr.addTask(txtTask.text, desc: txtDesc.text);
        // End the editing like touchesBegan override
        self.view.endEditing(true)
        // Clear out text
        txtTask.text = ""
        txtDesc.text = ""
        // Move to first view controller
        self.tabBarController?.selectedIndex = 0;
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    // UITextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder();
        
        return true
    }

}

