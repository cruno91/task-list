//
//  TaskManager.swift
//  Task List
//
//  Created by Chris Runo on 1/12/15.
//  Copyright (c) 2015 cron. All rights reserved.
//

import UIKit
// taskMgr is now a global variable instantiated from the TaskManager class
var taskMgr: TaskManager = TaskManager()
// task structure defines individual task with variables for name and description
struct task {
    // Default value of unnamed
    var name = "Un-Named"
    // Default value of undescribed
    var desc = "un-Described"
}

class TaskManager: NSObject {
    // creates empty array of struct task
    var tasks = [task]()
    // Defines function to add a task with name and description to the tasks array
    func addTask(name: String, desc: String) {
        // .append appends an element ot the array
        // in this case it is task (structure), which takes name and description
        tasks.append(task(name: name, desc: desc))
    }
    
}
